package com.example.sharelibrary;

import android.content.pm.ResolveInfo;

/**
 * Created by josue on 6/20/16.
 */
public interface OnSelectPackageListener {
    void onPackageSelect(String pPackage, ResolveInfo resolveInfo);
}
