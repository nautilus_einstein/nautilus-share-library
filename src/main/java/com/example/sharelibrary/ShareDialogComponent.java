package com.example.sharelibrary;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by josue on 6/20/16.
 *
 */
public class ShareDialogComponent {
    private final Context mContext;
    private final SheetAdapter mAdapter;
    private final View view;
    private BottomSheetDialog dialog;

    public ShareDialogComponent(Context pContext) {
        this.mContext = pContext;
        mAdapter = new SheetAdapter(pContext, getResolveList());
        view = View.inflate(mContext, R.layout.sheet_main, null);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(mContext, 4));
        recyclerView.setAdapter(mAdapter);

    }

    public void setOnPackageSelectListener(OnSelectPackageListener pOnPackageSelectListener){
        mAdapter.setOnItemClickListener(pOnPackageSelectListener);
    }

    public void show(){
        dialog = new BottomSheetDialog(mContext);
        if(view.getParent()!=null) {
            ((ViewGroup) view.getParent()).removeView(view);
        }
        dialog.setContentView(view);
        dialog.show();
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                dialog = null;
            }
        });

    }

    public void hide(){
        dialog.hide();
        dialog.dismiss();
    }

    private List<ResolveInfo> getResolveList(){
        Intent shareIntent=new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.setType("image/jpeg");
        return mContext.getPackageManager().queryIntentActivities(shareIntent, 0);
    }

    private List<String> loadPackagesList(){
        final List<String> packages=new ArrayList<>();
        Intent shareIntent=new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.setType("image/jpeg");
        String packageName;
        final List<ResolveInfo> resInfos = mContext.getPackageManager().queryIntentActivities(shareIntent, 0);
        if(!resInfos.isEmpty()) {
            for (ResolveInfo resInfo : resInfos) {
                packageName = resInfo.activityInfo.packageName;
                packages.add(packageName);

            }
        }

        return packages;
    }
}
